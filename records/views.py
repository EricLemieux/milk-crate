from django.shortcuts import render

from .models import Records

# Create your views here.
def index(request):
  records = Records.objects.all()[:10]

  context = {
    'name': 'Foo',
    'records': records
  }

  return render(request, 'records/listing.html', context)
